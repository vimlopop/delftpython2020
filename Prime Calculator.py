def prime_check(n):
    for i in range(2, n):  # not the most efficient, but checks all numbers up to the input
        if (n % i) == 0:  # if the remainder is 0 then i is a factor of toCheck
            break  # stops the process once it finds a factor
    else:
        print(n)


n = 2
while True:
    prime_check(n)  # checks if n is a prime and prints it if it is
    n += 1  # once the check is done, moves on to the next number
#   slowdown = input()
#  legit, just makes the user press enter to redo the loop

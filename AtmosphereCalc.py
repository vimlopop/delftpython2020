import math


def menu():
    print("1. Calculate ISA for altitude in metres")
    print("2. Calculate ISA for altitude in feet")
    print("3. Calculate ISA for altitude in Flight Levels")


def temp(alt):
    if alt <= 11000:
        return round(288.15 - (alt * 0.0065), 2)

    elif 11000 < alt <= 20000:
        return 216.65

    elif 20000 < alt <= 32000:
        return round(216.65 + ((alt - 20000) * 0.001), 2)

    elif 32000 < alt <= 47000:
        return round(228.65 + ((alt - 32000) * 0.0028), 2)

    elif 47000 < alt <= 51000:
        return 270.65

    elif 51000 < alt <= 71000:
        return round(270.65 - ((alt - 51000) * 0.0028), 2)

    elif 71000 < alt <= 86000:
        return round(214.65 - ((alt - 71000) * 0.002), 2)

    else:
        return "uhm, no"


def temp_celcius(kelvin):
    return round(kelvin - 273.15, 2)


def pressure(alt):
    if alt <= 11000:
        return round(101325 * math.pow((temp(alt) / 288.15), 5.25684803), 2)

    elif 11000 < alt <= 20000:
        return round(22632.1 * math.pow(math.e, ((-9.80665 / 62178.55) * (alt - 11000))), 2)

    elif 20000 < alt <= 32000:
        return round(5474.89 * math.pow((temp(alt) / 216.650), -34.1695121), 2)

    elif 32000 < alt <= 47000:
        return round(868.019 * math.pow((temp(alt) / 228.650), -12.20339721), 2)

    elif 47000 < alt <= 51000:
        return round(110.906 * math.pow(math.e, ((-9.80665 / 77676.55) * (alt - 47000))), 2)

    elif 51000 < alt <= 71000:
        return round(66.9389 * math.pow((temp(alt) / 270.65), 12.20339721), 2)

    elif 71000 < alt <= 86000:
        return round(3.95642 * math.pow((temp(alt) / 214.650), 18.0847561), 2)

    else:
        return "uhm, no"


def pressure_fraction(pascal):
    return round((pascal / 101325) * 100, 2)


def density(alt):
    return round(pressure(alt) / (287 * temp(alt)), 2)


def density_fraction(dense):
    return round((density(alt) / 1.225) * 100, 2)


menu()
option = int(input("Selection:  "))

if option == 1:
    alt = float(input("What kinda height we talking about here?  [m]"))

elif option == 2:
    alt = (float(input("What kinda height we talking about here?  [ft]"))) * 0.3048

elif option == 3:
    alt = (float(input("What kinda height we talking about here?  [FL]"))) * 30.48

else:
    alt = float(input("Input not recognised, please use metres in your input:"))

tempOutput = f"Temperature: {temp(alt)} [K] or {temp_celcius(temp(alt))} [°C]"
pressOutput = f"Pressure: {pressure(alt)} [Pa] or {pressure_fraction(pressure(alt))} % of Sea Level"
densityOutput = f"Density: {density(alt)} [kg/m³] or {density_fraction((density(alt)))} % of Sea Level"

if alt > 86000:
    print("Sorry, that's in space my dude")
else:
    print("  ")
    print(tempOutput)
    print(pressOutput)
    print(densityOutput)

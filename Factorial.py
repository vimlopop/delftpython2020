import math


def dumfac(value):
    if value == 0:
        return 1
    elif value < 0:
        return 0
    elif value == 1:
        return value
    else:
        return value * dumfac(value - 1)


value = int(input("Please input a number to be factorial'd:"))
result = math.factorial(value)
inter = f"{value} factorial is {result}."
interdum = f"Or, if you want it calculated more slowly we get; {dumfac(value)}"

if value < 0:
    print("That's not an interger my dude.")
else:
    print(inter)
    print(interdum)

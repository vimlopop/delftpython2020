""" 
Standard Dialog modules, using Tkinter

Modules:
    fileopen()  : returns filename fo scenariofile selected

Created by  : Jacco M. Hoekstra
Date        : October 2013

Modifation  :
By          :
Date        :

"""

from tkinter import *
import tkinter.filedialog
import os


"""
Created on Fri Oct 11 13:50:49 2013

Windows file open dialog

@author: jaccohoekstra
"""

def askopenfilename(name="Open File",ftypes=[("All files",".*")],folder="."):
   os.chdir(folder)

   master = Tk()
   #master.withdraw() #hiding tkinter window
   master.focus_set()

   file_path = tkinter.filedialog.askopenfilename(title=name,filetypes=ftypes)

# Close Tk, return to working directory    
   master.quit()
   
   os.chdir("..")

   return file_path

def asksavefilename(name="Save as",ftypes=[("All files",".*")],folder="."):

   os.chdir(folder)

   master = Tk()
   master.withdraw() #hiding tkinter window
   master.focus_set()

   file_handle = tkinter.filedialog.asksaveasfile(title=name,filetypes=ftypes)

   # Use this to write and close
   return file_handle

# PETER: I would recommend renaming your file so there's no space in the name. Spaces are a pain in the ass in filenames. 

import os

if not os.path.exists('animals.dat'):  # PETER: Style recommendation: be consistent with '' and "" as much as possible.
    print("Hold your onions! There appears to have been a problem")  # PETER: Don't tell your user that there has been "a problem" if you're going to recover and continue. Alert them to you doing an "alternative option".
    f = open('start.dat', 'r')
    rowAmount = f.read().splitlines()   # PETER: I don't like the naming here; splitlines does not return an "amount", it returns a list of strings.

else:
    f = open('animals.dat', 'r')
    rowAmount = f.read().splitlines()

table = []
i = 0

while i < len(rowAmount):  # PETER: Now that you know you've got this correct with a while loop, revisit the foreach loop. They're much cleaner, and once they click, they're easier and much less likely to make mistakes.
    line = rowAmount[i]
    column = line.split(" -- ")
    table.append(column)
    i = i + 1   # PETER: I wrote it like this when I was giving you a demo of how foreach works behind the scenes, but the Python way to do this is: i += 1

print(table)  # PETER: Remove this unless you're required

# PETER: "i" is a good name for a "disposable" variable whose only purpose is to start at 0 and increment. For things with actual meaning, it's better to give them a name that means something.
k = 0  # where k is the question index number

# PETER: If rowAmount has 10 items in it, len(rowAmount) will be 10. So if you try to call table[10], you'll crash, because indices start at 0 and end at len() - 1.
# PETER: In fact, because you always end a decision tree with a 'break', you can put this into an infinite loop: while(True)
while k <= len(rowAmount):
    # PETER: using a number directly in code (like the 1 here) is called a "magic number", and is typically a bad idea. Recommendation: create a variable somewhere near the top of the file with a meaningful name
    # PETER: Combine that with the "k" thing above, and table[current_question][question_text_index] is much easier for someone reading your code to understand than table[k][1]
    answer = input(table[k][1] + "?: ").lower()

    if answer == "yes" or answer == "y" or answer == "yeah":    # PETER: This is absolutely correct, but I personally think this looks cleaner: if answer in ["yes", "y", "yeah"]
        if not table[k][2].isnumeric():
            answer = input("Final guess, is it a " + table[k][2] + "?: ")   #
            if answer == "yes" or answer == "y" or answer == "yeah":
                print("Cool!")
                break
            else:
                print("That's a shame.")
                newAnimal = input("What was the correct animal?  ")
                newQuestion = input(f"Give a yes/no question to distinguish {newAnimal} from {table[k][2]}:  ")
                newAnswer = input(f"What was the answer for {newAnimal}?  ")


                # Need to update the table by adding a row and changing the action in the previous line
                if newAnswer.lower() == "yes" or newAnswer.lower() == "y" or newAnswer.lower() == "yeah":
                    newEntry = [str(len(rowAmount)), newQuestion, newAnimal, table[k][2]]
                else:
                    newEntry = [str(len(rowAmount)), newQuestion, table[k][2], newAnimal]

                table[k][2] = str(len(rowAmount))

                table.append(newEntry)

                # rewrite the whole table to animals.dat
                with open('animals.dat', 'w') as fout:

                    j = 0
                    while j <= len(rowAmount):
                        fout.write(" -- ".join(table[j]) + "\n")
                        j = j + 1

                break


        else:
            k = int(table[k][2])


    else:   # PETER: Again, not actually a problem, but your double-newlines weird me out
        if not table[k][3].isnumeric():
            answer = input("Final guess, is it a " + table[k][3] + "?: ")
            if answer == "yes" or answer == "y" or answer == "yeah":
                print("Cool!")
                break
            else:
                print("That's a shame.")
                newAnimal = input("What was the correct animal?  ")
                newQuestion = input(f"Give a yes/no question to distinguish {newAnimal} from {table[k][3]}:  ")
                newAnswer = input(f"What was the answer for {newAnimal}?  ")

                # Need to update the table by adding a row and changing the action in the previous line
                if newAnswer.lower() == "yes" or newAnswer.lower() == "y" or newAnswer.lower() == "yeah":
                    newEntry = [str(len(rowAmount)), newQuestion, newAnimal, table[k][3]]
                else:
                    newEntry = [str(len(rowAmount)), newQuestion, table[k][3], newAnimal]

                table[k][3] = str(len(rowAmount))

                table.append(newEntry)

                # rewrite the whole table to animals.dat
                with open('animals.dat', 'w') as fout:

                    j = 0
                    while j <= len(rowAmount):
                        fout.write(" -- ".join(table[j]) + "\n")
                        j = j + 1

                break

        else:
            k = int(table[k][3])

print(table)  # PETER: Also remove this

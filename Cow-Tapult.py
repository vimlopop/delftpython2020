from math import *
from matplotlib import pyplot as plot

R = 10.  # m
phi_start = 0.  # rad
phi_stop_deg = 60.  # degree
phi_stop = phi_stop_deg * pi / 180.  # rad
L_0 = 0.5  # m
k = 9000.  # N/m

g_0 = 9.80665  # m/s^2
m_cow = 550.  # kg
rho_cow = 1000.  # kg/m^3
rho_air = 1.225  # kg/m^3
c_D = 0.7

# Surface area of spherical cow, somehow

V_cow = m_cow / rho_cow
R_cow = ((3 / 4 * V_cow) / pi) ** (1 / 3)
S_cow = pi * R_cow ** 2


def catapult(R, phi_start, phi_stop, L_0, k, m_cow):
    phi = phi_start
    alpha = 0  # angular acceleration
    omega = 0  # angular velocity
    a = 0  # initial tangential acceleration
    velocity = 0
    dt = 0.0001

    t = 0
    x = -R
    y = 60

    tlst = []  # Lists to contain the data; time
    xlst = []  # Projectile distance
    ylst = []  # Projectile height
    Vlst = []  # Projectile velocity
    psilst = []  # Flight path Angle

    # Firing
    while phi < phi_stop:
        F_g = -m_cow * g_0
        L = R * cos(phi) / (sin((pi / 4) + (
                phi / 2)))
        F_k = k * (L - L_0)

        f_k_tangential = F_k * cos((pi / 4) - (phi / 2))
        f_g_tangential = F_g * cos(phi)
        f_t = f_k_tangential + f_g_tangential

        a = f_t / m_cow
        alpha = a / R
        omega = omega + alpha * dt
        velocity = omega * R
        phi = phi + omega * dt
        t = t + dt

        V_x = velocity * cos((pi/2) - phi)
        V_y = velocity * sin((pi/2)-phi)
        psi = atan2(V_y, V_x)
        x = x + V_x * dt
        y = y + V_y * dt

        tlst.append(t)
        xlst.append(x)
        ylst.append(y)
        Vlst.append(velocity)
        psilst.append(psi)

    # print(f"The max speed reached during the launch phase was: {round(Vlst[-1],3)} m/s")

    a_x = 0
    a_y = 0
    h = y
    s = x

    # Flight
    while h > 0:
        D_x = 0.5 * rho_air * (V_x ** 2) * c_D * S_cow * -(abs(V_x) / V_x)
        D_y = 0.5 * rho_air * (V_y ** 2) * c_D * S_cow * -(abs(V_y) / V_y)

        a_x = D_x / m_cow
        a_y = D_y / m_cow - g_0
        V_x = V_x + a_x * dt
        V_y = V_y + a_y * dt
        h = h + V_y * dt
        s = s + V_x * dt

        t = t + dt

        V = sqrt(V_x * V_x + V_y * V_y)
        psi = atan2(V_y, V_x)

        tlst.append(t)
        xlst.append(s)
        ylst.append(h)
        Vlst.append(V)
        psilst.append(psi)

    # print(f"Distance: {round(s,2)} Duration: {round(t,2)}")
    return tlst, xlst, ylst, Vlst, psilst


tlst, xlst, ylst, Vlst, psilst = catapult(R, phi_start, phi_stop, L_0, k, m_cow)

fig = plot.figure()

# First plot charts out the 2D path the Cow takes in flight

height_vs_distance = fig.add_subplot(221)
height_vs_distance.plot(xlst, ylst, "b")
plot.subplot(221)
plot.xlabel("Distance travelled in [m]")
plot.ylabel("Height [m]")
plot.title("Height vs Distance")

# Second plot charts the velocity ding the launch and firing phase

velocity_vs_time = fig.add_subplot(222)
velocity_vs_time.plot(tlst, Vlst, "r")
plot.subplot(222)
plot.xlabel("Time in [s]")
plot.ylabel("Velocity in [m/s]")
plot.title("Velocity vs Time")

# Third plot charts out the flight path angle psi

trajectory_vs_time = fig.add_subplot(223)
trajectory_vs_time.plot(tlst, psilst, "g")
plot.subplot(223)
plot.xlabel("Time in [s]")
plot.ylabel("Flight path angle in rad")
plot.title("Trajectory vs Time")

plot.subplots_adjust(left=None, bottom=0.1, right=None, top=None, wspace=0.3, hspace=0.5)
plot.show()

# Calculating k for 300m
prior_max = round(xlst[-1], 2)  # New variables because I'm about to overwrite the lists
prior_time = round(tlst[-1], 2)
klst = []
xklst = []
dk = 100
desired_distance = 300
while xlst[-1] < desired_distance:
    tlst, xlst, ylst, Vlst, psilst = catapult(R, phi_start, phi_stop, L_0, k, m_cow)
    x_opt = xlst[-1]
    k = k + dk
    klst.append(k)
    xklst.append(x_opt)
print(f"Maximum distance: {prior_max } m\nFlight Duration: {prior_time}  s")
print(f"To catapult the cow {desired_distance}m with a 50cm long elastic band, a spring constant of {int(k)} N/m is needed.")

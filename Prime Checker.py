toCheck = int(input("What number do you need to check my Boy?"))

if toCheck <= 1:
    print("dude, no, primes are integers greater than 1")
elif toCheck > 1:
    for i in range(2, toCheck):  # not the most efficient, but checks all numbers up to the input
        if (toCheck % i) == 0:  # if the remainder is 0 then i is a factor of toCheck
            print(toCheck, "is not a prime number")
            print(i, "times", toCheck // i, "is", toCheck)  # the lowest factor (i) * toCheck divided by i
            break   # stops the process once it finds a factor
    else:
        print(toCheck, "is a prime number")
else:
    print(toCheck, "is not a prime")

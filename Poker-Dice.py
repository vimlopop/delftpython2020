from random import randint

hand = []
player_hand = []
newplayer_hand = []

def roll(hand):
    ndice = 5 - len(hand)
    i = 0
    while i < ndice:
        single_die = randint(1, 6)
        hand.append(single_die)
        i = i + 1
    hand.sort()
    return hand


def calc_score(hand):
    unique = sorted(list(set(hand)))
    values = unique
    if values == [1, 2, 3, 4, 5]:
        score = 4
    elif values == [2, 3, 4, 5, 6]:
        score = 4
    elif len(values) == 1:
        score = 7
    elif len(values) == 5:
        score = 0
    elif len(values) == 4:
        score = 1
    elif len(values) == 2:
        if hand[1] == hand[3]:
            score = 6
        else:
            score = 5
    elif len(values) == 3:
        if hand[0] == hand[2] or hand[1] == hand[3] or hand[2] == hand[4]:
            score = 3
        else:
            score = 2
    else:
        score = -1

    return score


def select(hand):
    unique = set(hand)
    newhand = []
    single_die = randint(1, 6)

    if len(unique) == 5:
        # there are no pairs or whatnot
        if calc_score(hand) == 4:
            newhand = hand
        else:
            newhand.append(hand[0])
            newhand.append(hand[1])
            newhand.append(hand[2])
            newhand.append(hand[3])

    else:
        # pick all the pairs or better from the hand and discard the singles
        i = 0
        for i in range(5):
            if i == 0:
                if hand[i] == hand[i + 1]:
                    newhand.append(hand[i])
            elif i == 4:
                if hand[i] == hand[i - 1]:
                    newhand.append(hand[i])
            else:
                if hand[i] == hand[i - 1] or hand[i] == hand[i + 1]:
                    newhand.append(hand[i])

    newhand.sort()
    return newhand


print("Welcome to Dice Poker! Beat the computer by getting the highest score.\nRound 1:\n")
print(f"The computer rolled: {roll(hand)}")
print(f"And kept:  {select(hand)}")
hand = select(hand)
print(f"You rolled: {roll(player_hand)}")
print("Which dice do you want to keep? Choose from dice 1,2,3,4,5")
kept_dice = str(input("Your answer: "))

i=0
for i in kept_dice:
    newplayer_hand.append(roll(player_hand)[int(i)-1])

player_hand = newplayer_hand
newplayer_hand = []
print(f"You kept: {player_hand}")


print("\nRound 2:\n")
print(f"The computer rolled: {roll(hand)}")
print(f"And kept:  {select(hand)}")
hand = select(hand)
print(f"You rolled: {roll(player_hand)}")
print("Which dice do you want to keep? Choose from dice 1,2,3,4,5")
kept_dice = str(input("Your answer: "))

i=0
for i in kept_dice:
    newplayer_hand.append(roll(player_hand)[int(i)-1])

player_hand = newplayer_hand
newplayer_hand = []
print(f"You kept: {player_hand}")

'''
print("\nRound 3:\n")
print(f"The computer rolled: {roll(hand)}")
print(f"And kept:  {select(hand)}")
hand = select(hand)
print(f"You rolled: {roll(player_hand)}")
print("Which dice do you want to keep? Choose from dice 1,2,3,4,5")
kept_dice = str(input("Your answer: "))

i=0
for i in kept_dice:
    newplayer_hand.append(roll(player_hand)[int(i)-1])

player_hand = newplayer_hand
newplayer_hand = []
print(f"You kept: {player_hand}")
'''

print("\nFinal Rolls:\n")
hand = roll(hand)
print(f"The computer's final roll is: {hand}")
print(f"This gives the computer a final score of {calc_score(hand)}")
player_hand=roll(player_hand)
print(f"Your final roll is: {player_hand}")
print(f"This gives you a final score of {calc_score(player_hand)}")

if calc_score(hand) > calc_score(player_hand):
    print("The computer has won, better luck next time")
elif calc_score(hand) < calc_score(player_hand):
    print("Congratulations, you beat the rudimentary artificial intelligence!")
else:
    print("\nLooks like it's a tie. Play again?")
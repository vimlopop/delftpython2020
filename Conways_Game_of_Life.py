import pygame
import numpy as np
import random
from filedialogs import askopenfilename

# Colours
DEAD_COLOR = (0, 0, 0)
ALIVE_COLOR = (255, 255, 255)

# start from gen 0
generation = 0

# Ask user if they want a randomised pattern or a file
ans = input("Use random pattern? y/n: ")
frame_rate = int(input("How many new generations per second?"))

new_text = ""
intermediary_text = ""


def rle_converter(new_text):
    text = new_text
    no_whitespace = text.replace(" ", "")
    no_weirds = no_whitespace.replace("$\n", "$")
    new_lines = no_weirds.replace("$", "\n")
    lines = new_lines.splitlines()
    grid_dimensions = lines[0].split(",")
    x_dimension = int(grid_dimensions[0].replace("x=", ""))
    y_dimension = int(grid_dimensions[1].replace("y=", ""))
    del lines[0]

    outer_array = []
    for line in lines:
        if line.startswith("!"):
            break

        inner_array = []
        repetitions = 1
        for char in line:
            if not char.isnumeric():
                new_char = ""
                if char == "b":
                    new_char = "."
                elif char == "o":
                    new_char = "*"
                inner_array.append(new_char * repetitions)
                repetitions = 1
            elif repetitions == 1:
                repetitions = int(char)
            else:
                repetitions *= 10
                repetitions + int(char)
        outer_array.append("".join(inner_array))

    return x_dimension, y_dimension, "\n".join(outer_array)


if ans.lower() == "y":
    # Ask for the grid size, cell size and max speed
    ansSize = input("Please specify a grid size, a cell size and a speed (0 unbound) you would like to use"
                    " (X x Y x Size x Speed): ")

    # Create an array separated by X
    ansAr = ansSize.split("x")

    # Try using the input values else quit
    try:
        gridSize = (int(ansAr[1]), int(ansAr[0]))
        cellSize = int(ansAr[2])
        maxFPS = int(ansAr[3])
    except:
        print("Wrong input detected, closing...")
        quit()

    # Create 2 empty 2D arrays
    grid = np.zeros(shape=gridSize)
    next = np.zeros(shape=gridSize)

    # fill the grid with random values
    for i in range(0, gridSize[0]):
        for j in range(0, gridSize[1]):
            grid[i][j] = random.randint(0, 1)

elif ans.lower() == "n":

    # open file
    fileDir = askopenfilename()

    # Check if a file was correctly chosen, if so open file
    if not (fileDir == () or fileDir == None or fileDir == ""):
        file = open(fileDir, "r")

    else:
        print("No file chosen, closing")
        quit()

    inputList = []
    filteredList = []

    # for scoring the formats
    formatList = [0, 0, 0]  # c105, c106, cRLE

    for line in file.readlines():
        inputList.append(line.strip())

    # remove comment lines
    for line in range(len(inputList)):
        if not ((inputList[line][0] == "#" and inputList[line][1] == "D") or (
                inputList[line][0] == "#" and inputList[line][1] == "C") or (
                        inputList[line][0] == "#" and inputList[line][1] == "P") or (
                        inputList[line][0] == "#" and inputList[line][1] == "N") or (
                        inputList[line][0] == "#" and inputList[line][1] == "L") or (
                        inputList[line][0] == "#" and inputList[line][1] == " ")):
            filteredList.append(inputList[line])
        elif inputList[line][0] == "#" and inputList[line][1] == "D":
            formatList[0] += 10
        elif inputList[line][0] == "#" and inputList[line][1] == "C":
            formatList[2] += 10
        else:
            formatList[1] += 10

    # iterate through all the characters and figure out the format
    for line in range(len(filteredList)):
        for char in range(len(filteredList[line])):
            if filteredList[line][char] == "." or filteredList[line][char] == "*" or filteredList[line][char] == "o":
                formatList[0] += 1
            if filteredList[line][char] == "b" or filteredList[line][char] == "o" or filteredList[line][char] == "$":
                formatList[2] += 1
            if filteredList[line][char].isdigit():
                formatList[1] += 1

    # split the array up in words
    commands = []
    for line in range(len(filteredList)):
        commands.append(filteredList[line].split())

    # Get the longest command
    longestCom = 0
    for command in range(len(filteredList)):
        if longestCom < len(filteredList[command]):
            longestCom = len(filteredList[command])

    # print what format has been detected and fill the grid correctly
    if formatList[0] == max(formatList):
        print("1.05 format detected!")  # uses . for dead and * or o for alive

        # Default origin
        Origin = [0, 0]

        # Define the grid size

        comment_length = 0
        for entry in range(len(commands)):
            if commands[entry][0].startswith("#"):
                comment_length += 1
        # gridSize = (Origin[0]+50, Origin[1]+50)
        gridSize = (longestCom + 50, len(commands) - comment_length + 50)
        grid = np.zeros(shape=gridSize)
        # Command handler

        for command in range(len(commands)):
            if commands[command][0] == "#P":
                try:
                    Origin = [
                        abs(int((len(commands) - comment_length + 50 - (len(commands) - comment_length + 50 % 2)) / 2)),
                        abs((longestCom + 50 - (longestCom + 50 % 2)) / 2)]
                except:
                    Origin = [0, 0]
            if commands[command][0] == "#N":
                pass

            # Fill in the grid
            for line in range(len(filteredList) - comment_length):
                for char in range(len(filteredList[line])):
                    if filteredList[line][char] == "*" or filteredList[line][char] == "o":
                        grid[line + Origin[1]][char + Origin[0]] = 1

    elif formatList[1] == max(formatList):
        print("1.06 format detected!")  # uses coordinates to indicate alive
        # Get max value
        biggestVal = 0
        for command in range(len(commands)):
            for char in range(2):
                if int(commands[command][char]) > biggestVal:
                    biggestVal = int(commands[command][char])

        gridSize = (len(commands) + 20, biggestVal + 20)
        grid = np.zeros(shape=gridSize)

        # fill the grid
        for command in range(len(commands)):
            x = abs(int(commands[command][0]))
            y = abs(int(commands[command][1]))
            grid[y][x] = 1

    elif formatList[2] == max(formatList):
        print("RLE format detected!")
        for i in filteredList:
            intermediary_text += "\n" + i

        # Remove the blank lines so that the RLE converter can actually work, it's a bit hardcoded so, it's needed
        lines = intermediary_text.split("\n")
        non_empty_lines = [line for line in lines if line.strip() != ""]
        for line in non_empty_lines:
            new_text += line + "\n"

        familiar_format = rle_converter(new_text)[2]
        filteredList = familiar_format.split()

        gridSize = (rle_converter(new_text)[1], rle_converter(new_text)[0])
        grid = np.zeros(shape=gridSize)

        # Default origin
        Origin = [0, 0]

        # Actually populate the grid
        for line in range(len(filteredList)):
            for char in range(len(filteredList[line])):
                if filteredList[line][char] == "*":
                    grid[line + Origin[1]][char + Origin[0]] = 1




    else:
        print("unknown format")
        quit()

else:
    input("Unknown command, closing...")
    quit()

# Calculate the cell size only if a file was chosen
if ans.lower() == "n":
    cellSize = min(int(720 / gridSize[0]), int(1280 / gridSize[1]))
    if cellSize == 0:
        cellSize = 1
    # Define the max speed
    maxFPS = frame_rate

pygame.init()

screen = pygame.display.set_mode((cellSize * gridSize[1], cellSize * gridSize[0]))
fpsClock = pygame.time.Clock()


# Function that counts all the living neighbors
def countAlive(grid, x, y):
    """
    Count how many cells around the chosen cell are alive

    :param grid: 2D Array of the grid
    :param x: x position of the chosen cell
    :param y: y position of the chosen cell
    :return: integer value between 0-8
    """

    count_alive = 0
    for i in range(-1, 2):
        for j in range(-1, 2):
            # wrap around the board
            col = (x + i + gridSize[0]) % gridSize[0]
            row = (y + j + gridSize[1]) % gridSize[1]

            count_alive += grid[col][row]

    # if the cell we are looking at is alive subtract it from the alvie neighbors
    count_alive -= grid[x][y]
    return count_alive


# main loop
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            print("Quit by user action")
            pygame.quit()
            quit()

    # fill screen with dead color
    screen.fill(DEAD_COLOR)

    pygame.display.set_caption("Generation: " + str(generation))

    # loop through the grid and draw the correct color
    for i in range(0, gridSize[0]):
        for j in range(0, gridSize[1]):
            if grid[i][j] == 0:
                pygame.draw.rect(screen, DEAD_COLOR, (j * cellSize, i * cellSize, cellSize, cellSize), 1)
            elif grid[i][j] == 1:
                pygame.draw.rect(screen, ALIVE_COLOR, (j * cellSize, i * cellSize, cellSize, cellSize), 1)

    next = np.zeros(shape=gridSize)

    for i in range(0, gridSize[0]):
        for j in range(0, gridSize[1]):

            # get the state of the cell we are looking at
            state = grid[i][j]

            # Get the amount of alive neighbors
            aliveNeighbors = countAlive(grid, i, j)

            # if 3 neighbors are alive and the current state is dead then change state to alive
            if state == 0 and aliveNeighbors == 3:
                next[i][j] = 1
            elif state == 1 and (aliveNeighbors < 2 or aliveNeighbors > 3):
                next[i][j] = 0
            else:
                next[i][j] = grid[i][j]

    # If the arrays are not equal to eachother up the generation by 1
    # Else stop the program and display the finial generation at wich the pattern remains constant
    if not np.array_equal(grid, next):
        generation += 1
    else:
        print("The pattern stopped changing at generation: " + str(generation))
        break

    # Set the grid to the next grid
    grid = next

    # Update the display and make sure the tickspeed isnt higher then the maxFPS
    pygame.display.update()
    fpsClock.tick(maxFPS)

# Pause the program here to evaluate the pattern
input("Enter anything to quit")
pygame.quit()
quit()
